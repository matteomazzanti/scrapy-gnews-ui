# scrapy-gnews-ui

A UI to present the data collected with the web-crawler.
Scrapy-GNews is a web-crawler for GNews and Scrapy-GNews-UI is 
its friendly and gentle User-Interface.

## Requirements
- PHP ^7.2
- MongoDB ^1.5


## Install
- Clone the source code from git repository:   
  `$ git clone https://gitlab.com/taklia/scrapy-gnews-ui.git`
- Install dependencies   
  `$ cd src/`   
  `$ composer install`
- Copy default application settings:   
  `$ cp .env.example .env` 
- Set your application settings in the file `src/.env`.   
Set the parameter `DB_HOST` and `DB_PORT` if different 
from the default values.   
Set te parameters `SCRAPY_GNEWS_SCRAPY_APP`: define th relative 
path from public, to the Scrapy root folder
- Grant write permission to the webserver on the file `SCRAPY_GNEWS_SCRAPY_APP/settings.py`
- Set up a webserver to serve the folder `src/public` as web root folder. ``src/public/index.php`
- Open a browser at the web address in use by the webserver.

### Set you google credentials
- Under the menu Google Account, click on "Settings".
    - Fill the form with your email and passowrd.
    - FIll the form with your gnews cookie values:
        - Login into Google account and browse https://news.google.com.
        - Press F12, the developer tool window will appear.
        - Switch to the developer tool Network tab.
        - Click reload, a list of request will be shown.
        - Right click on the request news.google.com and select copy as cURL
        - Paste the value into the scrapy-gnews google-settings form.

## Use
The index page show the articles selected and the possible topics.   
You can mark the articles as "reviewed".


## TODO
- Stats on db (how many articles, how many reviewd etc.)