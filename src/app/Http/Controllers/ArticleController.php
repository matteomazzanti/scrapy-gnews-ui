<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Article;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        
        try {

            $validator = Validator::make($request->all(), [
                'topics' => 'required',
            ]);
            if ($validator->fails()) {
                throw new Exception($validator->messages()->first(), 1);
            }
            
            $params = $request->all();
            $articles = $this->loadArticles($params);
            $topics = $this->extractTopics($articles);
            
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th->getMessage());
        }
        
        return view('articles', [
            'articles' => $articles, 
            'topics' => $topics, 
        ]);
        
    }


    public function markreviewed($id)
    {
        $result = true;
        $httpStatusCode = 200;
        $message = '';

        try {

            $r = DB::collection('gnews')
                ->where('_id', $id)
                ->get();

            if ($r->isEmpty()) {

                $result = false;
                $httpStatusCode = 400;
                $message = 'Record not found.';

            } else {

                $r = DB::collection('gnews')
                    ->where('_id', $id)
                    ->update(['reviewed' => true]);

            }

        } catch (\Throwable $th) {
            //throw $th;
            $result = false;
            $httpStatusCode = 500;
            Log::error($th->getMessage());
        }

        return response()->json(['result' => $result, 
            'message' => $message], $httpStatusCode);
    }


    public function truncate()
    {
        $result = true;
        $httpStatusCode = 200;

        try {
            Article::truncate();
        } catch (\Throwable $th) {
            //throw $th;
            $result = false;
            $httpStatusCode = 500;
            Log::error($th->getMessage());
        }

        return response()->json(['result' => $result], $httpStatusCode);
    }



    private function loadArticles($params)
    {
        $query = DB::collection('gnews')
            ->orderBy('topic', 'asc')
            ->orderBy('publication_datetime', 'desc');

        if ('all' != $params['reviewed']) {
            $query->where('reviewed', ($params['reviewed'] === 'true'));
        }

        if (1 == $params['topics']) {
            $query->where('topic', $params['topics'][0]);
        } else {

            $query->where(function ($query) use ($params) {
                
                $query->where('topic', array_shift($params['topics']));
                foreach ($params['topics'] as $topic) {
                    $query->orWhere('topic', $topic);
                }
    
            });
        }

        $articles = $query->get();
        return $articles;
    }

    private function extractTopics($articles)
    {
        $topics = [];
        foreach ($articles as $article) {
            if (!in_array($article['topic'], $topics)) {
                $topics[] = $article['topic'];
            }
        }
        sort($topics);
        return $topics;
    }

}
