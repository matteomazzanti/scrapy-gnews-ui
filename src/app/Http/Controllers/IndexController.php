<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;



class IndexController extends Controller
{
    public function index()
    {
        $this->copyImages();

        $articles = json_decode($this->loadArticles(), true);
        $topics = $this->extractTopics($articles);

        $firstTopic = '';
        $filterArticles = [];
        if (!empty($articles)) {
            $firstTopic = $topics[0];
            $filterArticles = $this->filterArticlesByTopic($articles, $firstTopic);
        }

        return view('index', [
            'articles' => $filterArticles,
            'topics' => $topics,
            'selectedTopic' => $firstTopic
        ]);
    }


    private function loadArticles()
    {
        $articles = DB::collection('gnews')
            ->orderBy('topic', 'asc')
            ->orderBy('publication_datetime', 'desc')
            ->where('reviewed', false)
            ->get();
        return $articles;
    }


    private function extractTopics($articles)
    {
        $topics = [];
        foreach ($articles as $article) {
            if (!in_array($article['topic'], $topics)) {
                $topics[] = $article['topic'];
            }
        }
        sort($topics);
        return $topics;
    }

    private function copyImages() 
    {    
        $src = realpath( env('SCRAPY_GNEWS_SCRAPY_APP')) . '/../images/full';
        $dst = realpath( './images/articles/full');
        if ($src) { 
            $files = scandir($src);
            foreach ($files as $file) {
                if ($file != "." && $file != ".." && !file_exists("$dst/$file")) {
                    copy("$src/$file", "$dst/$file");
                }
            }
        } else {
            Log::error( 'IndexController::copyImages() cannot find directory.');
        }
    }

    private function filterArticlesByTopic($articles, $topic)
    {
        $filteredArticles = [];
        foreach ($articles as $article) {
            if ($article['topic'] == $topic) {
                $filteredArticles[] = $article;
            }
        }
        return $filteredArticles;
    }

    // Updates G_EMAIL, G_PASSWORD and G_COOKIES in `env('SCRAPY_GNEWS_SCRAPY_APP')./settings.py`
    public function updateGoogleSettings(Request $request)
    {
        $httpStatusCode = 200;
        $result = true;
        $validation = [];
        try {
            $validator = Validator::make($request->all(), [
            'email-address' => 'required|string|email',
            'password' => 'required|string|min:6',
            'google-cookie' => 'required|string|min:10',
            ]);
            if ($validator->fails()) {
                $result = false;
                $validations = $validator->messages();
                return redirect('/settings')->with('fail', 'Setting not done correctly:')->withErrors($validator);
            }
        }
        catch (\Throwable $th) {
            \report($th);
            $result = false;
            $httpStatusCode = 500;
    }
    // Change EMAIL, PASSSWORD and COOKIES in scraper settings
    $name_result = $this->swapLineInFile(env('SCRAPY_GNEWS_SCRAPY_APP'). '/settings.py',
                                        $request->input('email-address'),
                                        "G_EMAIL");

    $password_result = $this->swapLineInFile(env('SCRAPY_GNEWS_SCRAPY_APP'). '/settings.py',
                                        $request->input('password'),
                                        "G_PASSWORD");


    $dict = $this->curlToDict($request->input('google-cookie'));  // Convert cURL to python dict
    $cookie_result = $this->swapCookieInFile(env('SCRAPY_GNEWS_SCRAPY_APP') . '/settings.py',
                                      $dict,
                                      "G_COOKIES");
    // Verify if all writing operations have passed
    if ($cookie_result &&
        $name_result   &&
        $password_result) {
      return redirect()->action('IndexController@index')->with('success',
      'Google settings successfully set.');
    } else {
      return redirect('/settings')->with('fail',
      'Settings not successfully written to scraper. Please check ' .
      env('SCRAPY_GNEWS_SCRAPY_APP') . '/settings.py' .' manually.');
    }
  }

  private function curlToDict($curl) {
    $array = explode("-H", $curl);
    $cookie = "";
    foreach ($array as $index => &$value) {
        if (stripos($value, "cookie")) {
          $cookie = $value;
        }
    }

    $cookie = str_ireplace("Cookie: ", "", $cookie); // remove first header
    $cookie = substr($cookie, 2, -2); // Remove the first and last '

    // Subdivide into array with ; as delimter
    $fields = explode("; ", $cookie);

    // Get first occurance of = to make key => val
    $dict = "";
    foreach ($fields as $index => &$value) {
        $pos = strpos($value, '=');
        $keyPart = substr($value, 0, $pos);
        $valPart = substr($value, ($pos+1));
        $dict .= "'" . $keyPart . "': '" . $valPart . "'," . "\r\n";
    }

    return $dict;
  }

  private function swapLineInFile($url, $payload, $target) {
    $return = false;
    $content = file($url); // Read file data into an array
    foreach ($content as $lineNumber => &$lineContent) {
        if (strpos($lineContent, $target) !== false) {        // Find line of text we want
          $lineContent = $target . " = " ."'" .$payload. "'\r\n"; // Update line
          $resultString = implode("", $content);              // Put the array back into one string
          file_put_contents($url, $resultString);
          $return = true;
          break;
        }
    }
    return $return;
  }


  private function swapCookieInFile($url, $payload, $target) {
    $return = false;
    $gCookiesStartIndex = -1;
    $gCookiesFinishIndex = -1;
    $content = file($url);   // Read file data into an array
    $resultString = "";
    foreach ($content as $lineNumber => &$lineContent) {
        if (strpos($lineContent, $target) !== false) {// Find line of text we want
          $gCookiesStartIndex = $lineNumber;
        }
        if ($gCookiesStartIndex >= 0) { // We are inside the G_COOKIES text
          $pos = strpos($lineContent, '}');
          if ($pos !== false) {
            $gCookiesFinishIndex = $lineNumber;
            break;
          }
        }
    }

    if ($gCookiesStartIndex < $gCookiesFinishIndex) { // We have an interval to be replaced
      $payload_index = 0;
      $payload_array = explode("\r\n", $payload); // Make array of payload
      $limit = max(count($payload_array), $gCookiesFinishIndex);

      for ($i=$gCookiesStartIndex+1; $i < $limit; $i++) { // Replace lines with payload
        if (array_key_exists($payload_index, $payload_array)) {
          $content[$i] = "\t" . $payload_array[$payload_index++] . "\r\n"; // Add linebreak for readability
        } else {
          $content[$i] = "";  // if payload is done then erase remaining lines
        }
      }
      $resultString = implode("", $content); //Put the array back into one string
      file_put_contents($url, $resultString); // Write to file
      $return = true;
    }
    return $return;

  }
}
