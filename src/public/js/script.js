$( document ).ready(function() {

    $('span > a.badge').click( function( evt ) {
        evt.preventDefault();
    });


    /*
     * Not reviewd: on-off
     */
    $("[data-filter-review='not_reviewed']").click( function() {

        $("[data-filter-review='not_reviewed']")
            .toggleClass('badge-info');

        $("[data-filter-review='not_reviewed']")
            .toggleClass('badge-secondary');

        if( $("[data-filter-review='not_reviewed']")
                .hasClass('badge-secondary') &&
            $("[data-filter-review='reviewed']")
                .hasClass('badge-secondary') ) {

            $("[data-filter-review='not_reviewed']")
                .addClass('badge-info');

            $("[data-filter-review='not_reviewed']")
                .removeClass('badge-secondary');
        }
    });
    /*
     * Not reviewd: on-off
     */
    $("[data-filter-review='reviewed']").click( function() {

        $("[data-filter-review='reviewed']")
            .toggleClass('badge-info');

        $("[data-filter-review='reviewed']")
            .toggleClass('badge-secondary');

        if( $("[data-filter-review='not_reviewed']")
                .hasClass('badge-secondary') &&
            $("[data-filter-review='reviewed']")
                .hasClass('badge-secondary') ) {

            $("[data-filter-review='not_reviewed']")
                .addClass('badge-info');

            $("[data-filter-review='not_reviewed']")
                .removeClass('badge-secondary');
        }
    });


    /*
     * AND-OR: on-off
     */
    $("[data-filter-modality-topics='and']").click( function() {

        $("[data-filter-modality-topics='and']")
            .toggleClass('badge-info');

        $("[data-filter-modality-topics='and']")
            .toggleClass('badge-secondary');

        $("[data-filter-modality-topics='or']")
            .toggleClass('badge-info');

        $("[data-filter-modality-topics='or']")
            .toggleClass('badge-secondary');
    });
    /*
     * AND-OR: on-off
     */
    $("[data-filter-modality-topics='or']").click( function() {

        $("[data-filter-modality-topics='or']")
            .toggleClass('badge-info');

        $("[data-filter-modality-topics='or']")
            .toggleClass('badge-secondary');

        $("[data-filter-modality-topics='and']")
            .toggleClass('badge-info');

        $("[data-filter-modality-topics='and']")
            .toggleClass('badge-secondary');
    });


    /*
     * Deselect-All: on-off
     */
    $("[data-filter-deselect]").click( function() {

        $("[data-filter-topic]")
            .removeClass('badge-success');
        $("[data-filter-topic]")
            .addClass('badge-secondary');
    });




    /*
     * Topics badges on-off
     */
    $("[data-filter-topic]").click( function() {

        $( this ).toggleClass('badge-success');
        $( this ).toggleClass('badge-secondary');
        var el = this;

        if ( $("[data-filter-modality-topics='or']").hasClass('badge-info') ) {

            $("[data-filter-topic]").each(function( index ) {
                if (el != this) {
                    $( this ).removeClass('badge-success');
                    $( this ).addClass('badge-secondary');
                }
            });
        }
    });


    /*
     * AND-OR: topics-management: if or, only one topic badge-success
     */
    $("[data-filter-modality-topics='or']").click( function() {

        var found_active_el = false;
        $("[data-filter-topic]").each(function( index ) {

            //console.log($(this).html());

            if ( $(this).hasClass('badge-success')) {
                if (!found_active_el) {
                    found_active_el = true;
                } else {
                    $(this).addClass('badge-secondary');
                    $(this).removeClass('badge-success');
                }
            }
        });

    });


    /*
     * Query parameter collection
     */
    var getParamters = function() {

        // parameter: reviewed
        var reviewed = false;
        if ($("[data-filter-review='not_reviewed']").hasClass('badge-info')) {
            if ( $("[data-filter-review='reviewed']").hasClass('badge-info') ) {
                reviewed = 'all';
            } else {
                reviewed = false;
            }
        } else {
            reviewed = true;
        }

        // parameter: topics
        var topics = [];
        $("[data-filter-topic]").each(function( index ) {

            if ( $(this).hasClass('badge-success')) {
                topics.push( $(this).attr('data-filter-topic') );
            }
        });

        // return parameters
        return {
            'reviewed': reviewed,
            'topics': topics
        };
    }

    /**
     * Reloead articles
     */
    $('[data-action="reload-articles"]').click( function() {

        var parameters = getParamters();
        //console.log(parameters);

        if (0 == parameters.topics.length) {
            alert("No topic selected. \n"
            + 'Please select at least one topic.' );
        } else {

        $.ajax({
            url: "/api/article",
            method: "POST",
            data: parameters,
            cache: false
          })
            .done(function( html ) {
                $( "[data-context='section-topics']" ).html( html );
                mark_as_reviewed_buttons();
            });

        }
    });

    /**
     * Mark an article as reviewed
     */

    var mark_as_reviewed_buttons = function() {
        $('[data-action="mark-as-reviewed"]').click( function() {

            var article_id = $(this).attr('data-article-id');

            $.ajax({
                url: "/api/article/markreviewed/" + article_id,
                method: "GET",
                cache: false
              })
                .done(function( json ) {
                    $('li[data-article-id="' + article_id + '"]').toggle();
                    $('button[data-article-id="' + article_id + '"]').toggle();
                });

        });
    }
    mark_as_reviewed_buttons();




    /**
     * DB: truncate
     */
    $('[data-action="truncate"]').click( function() {

        var sure = confirm("Are you sure you want to reset the database? \n"
            + "All the article will be deleted.");
        if (sure == true) {
            $.ajax({
                url: "/api/article/truncate/",
                method: "GET",
                cache: false
            })
                .done(function( json ) {
                    location.reload();
                });
        }

    });

    /**
     * DB: crawl
     */
    $('[data-action="crawl"]').click( function() {

        var sure = confirm("Are you sure you want to run the spider? \n"
            + "New articles will be inserted.");
        if (sure == true) {
            $.ajax({
                url: "/api/article/test",
                method: "GET",
                cache: false,
                dataType: 'json',
                complete: function( json ) {

                    console.log(json);

                    $('#exampleModal').modal('toggle');

                }
            });
            /*
                .done(function( json ) {
                    console.log(json);

                    $('#exampleModal').modal('toggle');
                    readlog();
                });
            */
        }

    });


});
