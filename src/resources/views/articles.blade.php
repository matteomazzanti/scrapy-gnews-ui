@foreach ( $topics as $topic )

<div class="row" data-context='section-topic'>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div data-topic="{{ $topic }}">
            <div class="row" data-context='section-topic-title'>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h3>{{ $topic }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <ul class="list-unstyled">
                        
                        @foreach ($articles as $article)
                            @if ($article['topic'] == $topic)

                            <li class="media" data-article-id="{{ $article['_id'] }}">
                                @if (array_key_exists('images', $article))
                                    @if (1 <= count($article['images']))
                                    <img class="mr-3" 
                                        src="./images/articles/{{ $article['images'][0]['path'] }}" 
                                        alt="article image"
                                        width="200px" height="200px">
                                    @else
                                    <img class="mr-3" 
                                        src="./images/noimage.png" 
                                        alt="article image"
                                        width="200px" height="200px">
                                    @endif
                                @else
                                <img class="mr-3" 
                                    src=".../200x200" 
                                    alt="article image"
                                    width="200px" height="200px">
                                @endif
                                <div class="media-body">
                                    <h5 class="mt-0 mb-1">
                                        <a href="{{ $article['url'] }}" target="_blank">
                                            {{ $article['title'] }}
                                        </a>
                                    </h5>
                                    <p>
                                        @if (array_key_exists('publication_datetime', $article))
                                        <span data-tag="publication_datetime">
                                            <span class="bold">publication datetime: </span>
                                            <span>{{ $article['publication_datetime'] }}</span>
                                        </span><br />
                                        @endif
                                        <span data-tag="source">
                                            <span class="bold">source: </span>
                                            <span>{{ $article['source'] }}</span>
                                        </span><br />
                                        <span data-tag="gurl">
                                            <span class="bold">google url: </span>
                                            <span><a href="{{ $article['gurl'] }}" target="_blank">open</a></span>
                                        </span><br />
                                        <span data-tag="topic_type">
                                            <span class="bold">topic type: </span>
                                            <span>{{ $article['topic_type'] }}</span>
                                        </span><br />
                                        @if ($article['reviewed'])
                                        <span class="badge-size-3">
                                            <span class="badge badge-info">reviewed</span>
                                        </span>
                                        @endif
                                    </p>
                                    <div>
                                        @if (!$article['reviewed'])
                                        <button type="button" 
                                            class="btn btn-primary"
                                            data-action="mark-as-reviewed"
                                            data-article-id="{{ $article['_id'] }}">
                                            Mark as reviewed
                                        </button>
                                        @endif
                                    </div>
                                </div>
                            </li>

                            @endif
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
