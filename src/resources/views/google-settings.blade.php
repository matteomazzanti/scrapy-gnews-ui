@extends('layouts.default')
@section('title', 'Scrapy-GNews-UI')
@section('content')

<main class="login-form">
    <div class="cotainer">
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <span>{!! \Session::get('fail') !!}</span>
          <ul>
          @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
          @endforeach
          </ul>
      </div>
      @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Setup Google Settings</div>
                    <div class="card-body">
                        <form action="/settings" method="POST">
                          {{csrf_field()}}
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input type="text" id="email_address" class="form-control" name="email-address" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="password" class="form-control" name="password" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="google-cookie" class="col-md-4 col-form-label text-md-right">Google Cookie</label>
                                <div class="col-md-6">
                                    <textarea class="form-control rounded-0" id="google-cookie" name="google-cookie" rows="3"></textarea>
                                </div>
                            </div>

                            <!--
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div> -->

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

</main>
@endsection
<!-- Javascript -->
<!--
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous">
</script>
-->
<script src="js/jquery-3.4.1.min.js">
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
</script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
</script>

<script src="/js/script.js">
</script>
