@extends('layouts.default')
@section('title', 'Scrapy-GNews-UI')
@section('content')


<div class="container">
    @if (\Session::has('success'))
      <div class="alert alert-success">
          <span>{!! \Session::get('success') !!}</span>
    </div>
    @endif
    <div class="row" data-context='main-title'>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h1>Topics and Searches</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h2>Filters</h2>
        </div>
    </div>

    <div class="row" data-context='filter-review'>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <span class="badge-size-5">
                <a href="#"
                    class="badge badge-info"
                    data-filter-review="not_reviewed">Not reviewed</a>
                <a href="#"
                    class="badge badge-secondary"
                    data-filter-review="reviewed">Reviewed</a>
            </span>
        </div>
    </div>
    <div class="row" data-context='filter-modality-topics'>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <span>Modality for Topics and searches:</span>
            <span class="badge-size-4">
                <a href="#"
                    class="badge badge-secondary"
                    data-filter-modality-topics="and">AND</a>
                <a href="#"
                    class="badge badge-info"
                    data-filter-modality-topics="or">OR</a>
                    &nbsp; &nbsp; &nbsp;
                <a href="#"
                    class="badge badge-secondary"
                    data-filter-deselect="all">Deselect all</a>
            </span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">

            <span class="badge-size-3">
            @foreach ($topics as $topic)

                @if ($topic == $selectedTopic)
                <a href="#"
                    class="badge badge-success"
                    data-filter-topic="{{ $topic }}">{{ $topic }}</a>
                @else
                <a href="#"
                    class="badge badge-secondary"
                    data-filter-topic="{{ $topic }}">{{ $topic }}</a>
                @endif

            @endforeach
            </span>

        </div>
    </div>

    <div class="row" data-context='section-reload-articles'>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <button type="button"
                class="btn btn-primary"
                data-action="reload-articles">
                Reload articles
            </button>
        </div>
    </div>

    <div class="row" data-context='section-articles-title'>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h1>Articles</h1>
        </div>
    </div>

    <div data-context="section-topics">
    <div class="row" data-context='section-topic' data-topic="{{ $selectedTopic }}">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">

            <div class="row" data-context='section-topic-title'>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h3>{{ $selectedTopic }}</h3>
                </div>
            </div>

            @if (empty($articles))
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <span>No article to show.</span>
                </div>
            </div>
            @endif

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <ul class="list-unstyled">

                        @foreach ($articles as $article)
                            @if ($article['topic'] == $selectedTopic)

                            <li class="media" data-article-id="{{ $article['_id']['$oid'] }}">
                                @if (array_key_exists('images', $article))
                                    @if (1 <= count($article['images']))
                                    <img class="mr-3"
                                        src="./images/articles/{{ $article['images'][0]['path'] }}"
                                        alt="article image"
                                        width="200px" height="200px">
                                    @else
                                    <img class="mr-3"
                                        src="./images/noimage.png"
                                        alt="article image"
                                        width="200px" height="200px">
                                    @endif
                                @else
                                <img class="mr-3"
                                    src=".../200x200"
                                    alt="article image"
                                    width="200px" height="200px">
                                @endif
                                <div class="media-body">
                                    <h5 class="mt-0 mb-1">
                                        <a href="{{ $article['url'] }}" target="_blank">
                                            {{ $article['title'] }}
                                        </a>
                                    </h5>
                                    <p>
                                        @if (array_key_exists('publication_datetime', $article))
                                        <span data-tag="publication_datetime">
                                            <span class="bold">publication datetime: </span>
                                            <span>{{ $article['publication_datetime'] }}</span>
                                        </span><br />
                                        @endif
                                        <span data-tag="source">
                                            <span class="bold">source: </span>
                                            <span>{{ $article['source'] }}</span>
                                        </span><br />
                                        <span data-tag="gurl">
                                            <span class="bold">google url: </span>
                                            <span><a href="{{ $article['gurl'] }}" target="_blank">open</a></span>
                                        </span><br />
                                        <span data-tag="topic_type">
                                            <span class="bold">topic type: </span>
                                            <span>{{ $article['topic_type'] }}</span>
                                        </span><br />
                                        @if ($article['reviewed'])
                                        <span class="badge-size-3">
                                            <span class="badge badge-info">reviewed</span>
                                        </span>
                                        @else
                                        <span class="badge-size-3 displaynone"
                                            data-context="badge-reviewed"
                                            data-article-id="{{ $article['_id']['$oid'] }}">
                                            <span class="badge badge-info">reviewed</span>
                                        </span>
                                        @endif
                                    </p>
                                    <div>
                                        @if (!$article['reviewed'])
                                        <button type="button"
                                            class="btn btn-primary"
                                            data-action="mark-as-reviewed"
                                            data-article-id="{{ $article['_id']['$oid'] }}">
                                            Mark as reviewed
                                        </button>
                                        @endif
                                    </div>
                                </div>
                            </li>

                            @endif
                        @endforeach

                    </ul>
                </div>
            </div>

        </div>
    </div>
    </div>

</div>



<!-- Modal -->
<div class="modal fade"
    id="exampleModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <pre id="output"></pre>
        </div>
        <div class="modal-footer">
            <button type="button"
                class="btn btn-secondary"
                data-dismiss="modal">
                Close
            </button>
        </div>
        </div>
    </div>
</div>

@endsection
