<html lang="en">
    <head>
          <meta charset="utf-8">
          <meta name="description" content="The HTML5 Herald">
          <meta name="author" content="SitePoint">

          <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
          <link rel="stylesheet" href="css/style.css?v=1.0">

          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

          <title>@yield('title')</title>
    </head>
    <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
            <a class="navbar-brand" href="/">Scrapy-GNews-UI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExample09">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"
                            href="https://example.com"
                            id="dropdown09"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">My Google Account</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown09">
                            <a class="nav-link"
                                href="{{ url('settings') }}">Edit Settings</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"
                            href="https://example.com"
                            id="dropdown09"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">My topics</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown09">
                            <a class="nav-link"
                                href="https://news.google.com/my/library"
                                target="_blanc">Library</a>
                            <a class="nav-link"
                                href="https://news.google.com/my/searches"
                                target="_blanc">Custom searches</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"
                            href="https://example.com"
                            id="dropdown09"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">Articles</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown09">
                            <a class="nav-link"
                                href="#"
                                data-action="truncate">Reset the DB</a>
                            <!--
                            <a class="nav-link"
                                href="#"
                                data-action="crawl">Run the Spider</a>
                            -->
                        </div>
                    </li>

                    <!--
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"
                            href="https://example.com"
                            id="dropdown09"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">Dropdown</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown09">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    -->
                </ul>
                <form class="form-inline my-2 my-md-0">
                    <input class="form-control" type="text" placeholder="Search in titles" aria-label="Search">
                </form>
            </div>
        </nav>


        @yield('content')


        <!-- Javascript -->
        <!--
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous">
        </script>
        -->
        <script src="js/jquery-3.4.1.min.js">
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
        </script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
        </script>

        <script src="/js/script.js">
        </script>
    </body>
</html>
