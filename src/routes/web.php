<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::get('/', function () {
#    return view('index');
#});

Route::get('/', 'IndexController@index');

Route::get("/settings", function() {
   return View::make("google-settings");
});

Route::post("/settings", 'IndexController@updateGoogleSettings');
